﻿// Matías Leandro Flores

/* Función para calcular el número correspondiente de la secuencia de Fibonacci
para un número dado. La función debe poder tomar un número
y retornar el número de la secuencia de ese índice sin usar bucles.
*/

static int Fibo(int n){  // Solo se puede calcular para enteros positivos (unsigned)
    if (n < 0){
        Console.WriteLine("Debe ingresar un número positivo");
        return 0;
    }
    if (n <= 1){
        return n;  // si n = 1 retorna 1, si n = 0 retorna 0
    }
    else {
        return Fibo(n-1) + Fibo(n-2);  // num de secuencia es suma de los dos anteriores
    }
}

// Llamada de la función
Console.Write("Ingrese un número: ");
int n = Convert.ToInt32(Console.ReadLine());  
int Fibonacci = Fibo(n);
Console.WriteLine("El resultado de la secuencia de Fibonacci para " + n + " es: " + Fibonacci);
