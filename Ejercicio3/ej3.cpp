// Ejercicio3 Matías Leandro Flores

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

struct Cliente{
    int cedula;
    int telefono;
    char *nombre;
    char *empresa;
};

int cantidad_clientes = 5;  // 5 clientes según empresario
Cliente *clientes = new Cliente[cantidad_clientes];

void llenar_cliente(Cliente *&cliente_reserva, int cedula_ingresada = 0, int telefono_ingresado = 0, char *nombre_ingresado = "", 
    char *empresa_ingresada = ""){
    // Revisión de datos obligatorios
    if (cedula_ingresada == 0){
        cout << "[Error] Debe ingresar el dato del cliente: cedula" << endl;
    }
    else if (telefono_ingresado == 0){
        cout << "[Error] Debe ingresar el dato del cliente: telefono" << endl;

    }
    else if (nombre_ingresado == ""){
        cout << "[Error] Debe ingresar el dato del cliente: nombre" << endl;
    }

    // Llenado de atributos de Cliente
    cliente_reserva->cedula = cedula_ingresada;
    (*cliente_reserva).telefono = telefono_ingresado;

    // alocar memoria para nombre y empresa y copia de strings a atributo
    int largo_nombre = strlen(nombre_ingresado);
    int largo_empresa = strlen(empresa_ingresada);
    cliente_reserva->nombre = new char[largo_nombre];
    (*cliente_reserva).empresa = new char[largo_empresa];
    strcpy((*cliente_reserva).nombre, nombre_ingresado);
    strcpy((*cliente_reserva).empresa, empresa_ingresada);
}

// Función para mostrar la información de un cliente 
void mostrar_info(Cliente *cliente){
    cout << "Cliente: " << (*cliente).nombre << endl;
    cout << "Empresa: " << (*cliente).empresa << endl;
    cout << "Cédula: " << (*cliente).cedula << endl;
    cout << "Teléfono: " << (*cliente).telefono << endl;
    cout << endl;  // Para dejar un espacio en caso de mostrar múltiples clientes
}

// Función buscar cliente según cédula
Cliente *buscar_cliente(Cliente *&clientes, int cedula, int cantidad_clientes){
    for (int i = 0; i < cantidad_clientes; i++)
    {
        if (clientes[i].cedula == cedula){
            return &clientes[i];
        }
    }
    return nullptr;
}

void liberar(Cliente *&cliente, int cantidad_clientes){
    for (int i = 0; i < cantidad_clientes; i++)
    {
        delete cliente[i].nombre;
        delete cliente[i].empresa;
    }
}

int main(){
    // Variables para guardar atributos de clientes dados por usario
    char nombre[50];  // Arreglo de 20 chars no alocado en heap (que se libera al terminar main())
    char empresa[50];
    char cedula_in[10];
    char telefono_in[10];
    int cedula;
    int telefono;
    int cedula_buscar;
    

    // Para llenar clientes
    Cliente *ptr_cliente;  // Puntero a cliente actual a rellenar
    for (int i = 0; i < cantidad_clientes; i++)
    {
        ptr_cliente = &clientes[i];  // Actualiza puntero a siguiente cliente

        // Atributos para cada cliente ingresados por el usuario
        cout << "Ingrese un nombre para el cliente " << i << endl;
        // Se usa cin.getline para poder ingresar nombres o empresas con espacios
        cin.getline(nombre, 50, '\n');
        cout << "Ingrese una empresa para el cliente " << i << endl;
        cin.getline(empresa, 50, '\n');
        cout << "Ingrese un numero de telefono para el cliente " << i << endl;
        cin.getline(telefono_in, 50, '\n');
        telefono = stoi(telefono_in);
        cout << "Ingrese una cedula para el cliente " << i << endl;
        cin.getline(cedula_in, 50, '\n');
        cedula = stoi(cedula_in);

        llenar_cliente(ptr_cliente, cedula, telefono, nombre, empresa); 

    }

    // Para mostrar clientes
    for (int i = 0; i < cantidad_clientes; i++)
    {
        mostrar_info(&clientes[i]);
    }

    cout << "Ingrese una cédula a buscar: " << endl;
    cin >> cedula_buscar;
    // Buscar un cliente (en este ejemplo se busca por la cédula del segundo cliente)
    Cliente *cliente_buscar = buscar_cliente(clientes, cedula_buscar, cantidad_clientes);
    if (cliente_buscar != nullptr){
        cout << "Cliente encontrado" << endl;
        mostrar_info(cliente_buscar);
    }
    else{
        cout << "Cliente no encontrado" << endl;
    }
    
    // Liberar memoria de nombre y empresa de cada cliente
    liberar(clientes, cantidad_clientes);

    // Finalmente, liberar puntero a secuencia de clientes
    delete[] clientes;

}



