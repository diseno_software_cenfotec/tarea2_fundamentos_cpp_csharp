// Ejercicio2 Matías Leandro Flores
#include <iostream>

using namespace std;

// Declaración de array números alocado en heap
int *numeros = new int[10];

// Función que cambie signo a negativos
void makePositive(int *&numeros){    // Pasar numeros como referencia para que no haga una copia
    for (int i = 0; i < 10; i++)
    {
        if (*(numeros+i) < 0){  // Suma a puntero para recorrer secuencia buscando negativos
            *(numeros+i)*=-1;   
        }
    }
}


int main(){
    // Poblar 'numeros' 
    *numeros = 1;
    *(numeros+1) = -2;
    *(numeros+2) = -3;
    *(numeros+3) = 4;
    *(numeros+4) = -5;
    *(numeros+5) = 6;
    *(numeros+6) = -7;
    *(numeros+7) = -2;
    *(numeros+8) = 9;
    *(numeros+9) = -10;
    
    // Mostrar vector de 10 enteros previo a llamada de funcion
    for (int i = 0; i < 10; i++){
        cout << "Antes: "<< *(numeros+i) << endl;
    }
    makePositive(numeros); // Hacer que los negativos sean positivos
    for (int i = 0; i < 10; i++){
        cout << "Despues: "<< *(numeros+i) << endl;
    }

    delete[] numeros;
    
}
